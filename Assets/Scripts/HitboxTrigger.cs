﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxTrigger : MonoBehaviour
{
    [Header("Settings")]
    public Animator animator;
    public bool triggered = false;
    


    private void Update()
    {
        
        animator.SetBool("triggered", triggered);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        triggered = true;
        print("Animation wird gestartet");
        
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        triggered = false;
        print("Animation wird gestoppt");
    }

}
