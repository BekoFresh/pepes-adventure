﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxTrigger_Stone : MonoBehaviour
{
    [Header("Settings")]
    public Animator animator;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && animator.IsInTransition(0))
        {
            animator.Play("SpikeHead_Hit");
            
        }
        else
        {
            animator.Play("SpikeHead_Idle");
        }

        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }

}