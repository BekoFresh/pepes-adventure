﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getCoin : MonoBehaviour
{
    int coins;
    
    private void Update()
    {
        coins = PlayerPrefs.GetInt("coins");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        coins++;
        Destroy(this.gameObject);
        PlayerPrefs.SetInt("coins", coins);

    }

}
